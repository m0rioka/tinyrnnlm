#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .tinyrnnlm import TinyCRnnLM
from .tinyrnnlm import perplexity
