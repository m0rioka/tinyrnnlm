# TinyRnnLM

tiny wrapper for CRnnLM (in [RNNLM Toolkit](http://rnnlm.org)).

---

the following functions are supported.

* read the rnnlm model dumped by **PLAIN TEXT**
* forward propagation
* compute test-set perplexity

